# Spotipy

Spotipy is a music streaming app written in Python using FastAPI, Jinja2, and a SQLite database.
It aims to be minimal, simple and therefore easy to maintain and interact with.