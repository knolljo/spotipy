# Planung

## Programmiersprache

- Python 3.11.3
Dies ist die aktuellste Version, sie verspricht gesteigerte Performance und besser beschriebenen Error-Tracebacks.

- pip
Als Packaging-Tool wird `pip` verwendet. Dieses Nutzt Metadaten aus der Datei `pyproject.toml`.
Dort werden Abhaengigkeiten spezifiziert und ein build-system angegeben.
Dieses Build-System erzeugt aus unserem Python Sourcecode eine `wheel`-Datei.
Wheel-Dateien sind ein einheitlich spezifiziertes Format mit dem sich ueber Pythons Paketmanager Pakete installieren lassen.
Der Sourcecode und die Paket-Metadaten werden dabei in eine einzige Datei zusammengepakt.
Dies erleichtert die verteibarkeit.

## Tests

- pytest
In Funktionen werden Test-Cases spezifiziert,
diese koennen in einer Setup-Phase vorbereitet und in einer Teardown-Phase nachbearbeitet werden.
Es wird der Ansatz des Test-Driven-Developments verfolgt, wo moeglich,
diesen jedoch stringent einzuhalten ist nicht immer sinnvoll.

## Backend

- Fast-API
Bessere Performance im Vergleich zu anderen Frameworks, async faehig.

## Templating-Engines

- Jinja2
Einfache Syntax und gute Integration mit FastAPI.

## Application-State/Datenbanken

- SQLite
Relationale Datenbank mit guter Performance und Ressourcenverbrauch, fuer ein
Projekt dieser groesse gut skalierbar.

- Filesystem
Das Speichern von Musik erfolgt auf dem Filesystem

## Frontend

- HTML5
- PicoCSS
PicoCss ist ein CSS-Framework das sehr einfach zu handhaben ist, es enthaelt 
auch eine classless Version die ganz ohne Klassen im HTML-Text auskommt.

## Entwicklung

- VCS: git
- IDE: helix
- LSP-Server: pylsp-mypy
- Linter: ruff
- Formatter: black
- Tests: hurl
