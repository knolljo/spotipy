# Einleitung

**Spotipy** ist eine Musikstreaming App die es Nutzern ermoeglicht Musik zu hoeren, aber auch eigene Musik hochzuladen und zu teilen.

Anforderungen sind:
- Nutzeranmeldung/registrierung
- Musik Upload
- Musik Stream
- Musik Suche
- Wiedergabelisten

Die Planung der App erfolgte in den Phasen:
- Vorueberlegungen zu den Anforderungen
- Entwurf der Schnittstellen und des Datenmodells
- Implementierung und paralleles Testen

Das abschliessend gewaehlte Stack besteht aus:
- Web-Framework: **FastAPI**
- Datenbank: **SQLite**
- ORM: **sqlmodel**
- Templating: **Jinja2**
- Styling: **PicoCSS**