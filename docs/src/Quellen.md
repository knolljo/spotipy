# Quellenverzeichniss

## Docs
- Dokumentationssoftware [mdbook](https://github.com/rust-lang/mdBook)

## Libraries

- Web-Framework [FastAPI](https://fastapi.tiangolo.com/)
- Datenbank [SQLite](https://www.sqlite.org/)
- ORM [SQLModel](https://sqlmodel.tiangolo.com/)
- CSS-Framework [PicoCSS](https://picocss.com/)

## Development

- [git](https://git-scm.com/)
- [helix](https://helix-editor.com/)
- [pylsp-mypy](https://pypi.org/project/pylsp-mypy/)
- [ruff](https://beta.ruff.rs/)
- [black](https://github.com/psf/black)
- [hurl](https://hurl.dev/)
