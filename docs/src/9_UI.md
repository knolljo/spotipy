# User Interface

Bei Web-Frontends kann man zwischen drei Kategorien unterscheiden:

- SSG: Static Site Generation
- SSR: Server Side Rendering
- SPA: Single Page Application

In disem Projekt wird Server Side Rendering verwendet, mithilfe von JavaScript
kann man dennoch dynamische Seiten bauen.