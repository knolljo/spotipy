# Summary

[Einleitung](1_Einleitung.md)

# Entwicklungsphasen

- [Planung](2_Planung.md)
- [Datenmodell](3_Datenmodell.md)

# Funktionalitaet

- [User](4_User.md)
- [Upload](5_MusikUpload.md)
- [Stream](6_MusikStream.md)
- [Suche](7_Musiksuche.md)
- [Playlists](8_Playlists.md)

# Guide

- [Installation](Installation.md)

-----------

[Quellen](Quellen.md)
