"""

Spotipy is a music streaming app written in Python using FastAPI, Jinja2,
and a SQLite database.
It aims to be minimal, simple and therefore easy to maintain and interact with.

Examples:
    >>> spotipy.run(host="127.0.0.1", port=8000)

Run:
    $ python -m spotipy

"""
