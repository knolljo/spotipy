"""Spotipy main application"""
from fastapi import FastAPI, status
from fastapi.staticfiles import StaticFiles

from . import db
from .api import albums, auth, tracks, users, view


def startup():
    """Startup event handler"""
    db.create_tables()


def create_app() -> FastAPI:
    """Create a spotipy instance"""
    app = FastAPI()

    # Add exception handlers
    app.add_exception_handler(status.HTTP_401_UNAUTHORIZED, view.unauthorized)
    app.add_exception_handler(status.HTTP_404_NOT_FOUND, view.notfound)

    # Mounts (css, js)
    app.mount("/static", StaticFiles(directory="static"), name="static")
    app.mount("/audio", StaticFiles(directory="audio"), name="audio")
    # Routers
    app.include_router(auth.router)
    app.include_router(albums.router)
    app.include_router(users.router)
    app.include_router(tracks.router)
    app.include_router(view.router)

    # Register the startup event handler
    app.add_event_handler("startup", startup)

    return app
