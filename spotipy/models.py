"""Database models of Spotipy"""
from datetime import datetime
from typing import Optional

from pydantic import BaseModel
from sqlmodel import Field, Relationship, SQLModel


class BaseUser(SQLModel):
    username: str = Field(unique=True, max_length=50, primary_key=True)
    display_name: str = Field(max_length=50)


class User(BaseUser, table=True):
    hashed_password: str

    albums: list["Album"] = Relationship(back_populates="user")
    tracks: list["Track"] = Relationship(back_populates="user")


class UserCreate(BaseUser):
    password: str = Field(unique=True, max_length=100)


class UserRead(BaseUser):
    username: str


class TokenData(BaseModel):
    username: Optional[str] = None


class BaseAlbum(SQLModel):
    title: str
    release_date: datetime = Field(default_factory=datetime.now)
    username: str = Field(default=None, foreign_key="user.username")


class Album(BaseAlbum, table=True):
    id: Optional[int] = Field(default=None, primary_key=True, nullable=False)

    user: Optional["User"] = Relationship(back_populates="albums")
    tracks: list["Track"] = Relationship(back_populates="album")


class AlbumCreate(BaseAlbum):
    pass


class AlbumRead(BaseAlbum):
    id: int


class BaseTrack(SQLModel):
    title: str
    audio_url: str

    username: str = Field(default=None, foreign_key="user.username")
    album_id: Optional[int] = Field(default=None, foreign_key="album.id")


class Track(BaseTrack, table=True):
    id: Optional[int] = Field(default=None, primary_key=True, nullable=False)

    user: Optional["User"] = Relationship(back_populates="tracks")
    album: Optional["Album"] = Relationship(back_populates="tracks")


class TrackCreate(BaseTrack):
    pass


class TrackRead(BaseTrack):
    id: int


class TrackSearchEntry(BaseModel):
    id: int
    title: str
