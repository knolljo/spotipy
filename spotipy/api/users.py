"""Spotipy routes for users"""
from fastapi import APIRouter, Depends
from sqlmodel import Session, select

from .. import db
from ..models import User

router = APIRouter()


@router.get("/api/users", response_model=list[str])
async def get_users(session: Session = Depends(db.get_session)):
    """Get all users names"""
    return [user.username for user in session.exec(select(User)).all()]
