"""Spotipy routes for tracks"""
from typing import Annotated

from fastapi import APIRouter, Depends, File, Form, UploadFile, status
from fastapi.responses import RedirectResponse
from sqlmodel import Session
from thefuzz import process
from thefuzz.utils import full_process

from .. import db
from ..models import Track, TrackRead, TrackCreate, UserRead
from .core import get_all_or_404, get_single_or_404, get_user_required, save_file

router = APIRouter()


@router.get("/api/tracks", response_model=list[TrackRead])
async def get_tracks(session: Session = Depends(db.get_session)):
    return get_all_or_404(session, Track)


@router.get("/api/tracks/{track_id}", response_model=TrackRead)
async def get_track(track_id: int, session: Session = Depends(db.get_session)):
    return get_single_or_404(session, Track, track_id)


@router.post("/api/tracks", response_class=RedirectResponse, status_code=201)
async def create_track(
    title: Annotated[str, Form()],
    file: UploadFile = File(...),
    session: Session = Depends(db.get_session),
    user: UserRead = Depends(get_user_required),
):
    path = await save_file(file)
    print("=" * 20, path.name)
    new_track = TrackCreate(
        title=title,
        audio_url=path.name,
        username=user.username,
    )
    db_track = Track.from_orm(new_track)
    session.add(db_track)
    session.commit()
    return RedirectResponse(
        url=f"/tracks/{db_track.id}", status_code=status.HTTP_302_FOUND
    )


def fuzz_processor(s: Track | str):
    if isinstance(s, Track):
        return full_process(s.title)
    return full_process(s)


@router.get("/api/search")
async def search_tracks(title: str, session: Session = Depends(db.get_session)):
    tracks = await get_tracks(session)
    matches = process.extractBests(
        title, tracks, processor=fuzz_processor, score_cutoff=50
    )
    return [match[0] for match in matches]
