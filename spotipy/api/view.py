"""Spotipy web-ui"""
import functools

from fastapi import APIRouter, Depends, Request, status
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates
from sqlmodel import Session, select

from .. import db
from ..models import Album, User, UserRead, Track
from .albums import get_albums
from .core import get_single_or_404, get_user_optional, get_user_required
from .tracks import get_track, get_tracks


def template(template_path: str):
    def decorator(func):
        @functools.wraps(func)
        async def wrapper(*args, **kwargs):
            print(template_path, args, kwargs)
            result = await func(*args, **kwargs)
            result = result if result is not None else {}

            data = {
                "user": get_user_optional(kwargs["request"]),
                "request": kwargs["request"],
                **result,
            }

            return TemplateResponse(template_path, data)

        return wrapper

    return decorator

    return decorator


router = APIRouter()

templates = Jinja2Templates(directory="templates")
TemplateResponse = templates.TemplateResponse


def unauthorized(request: Request, exc):
    return TemplateResponse(
        "unauthorized.html", {"request": request, "detail": exc.detail}
    )


def notfound(request: Request, exc):
    return TemplateResponse("notfound.html", {"request": request, "detail": exc.detail})


@router.get("/", response_class=HTMLResponse)
@template("index.html")
async def index(request: Request, user=Depends(get_user_optional)):
    pass


@router.get("/register")
def register_page(request: Request):
    # TODO static
    return TemplateResponse("register.html", {"request": request})


@router.get("/login", response_class=HTMLResponse)
async def login_page(request: Request):
    # TODO static
    return TemplateResponse("login.html", {"request": request})


@router.get("/logout", response_class=RedirectResponse)
async def logout():
    response = RedirectResponse(url="/login", status_code=status.HTTP_302_FOUND)
    response.delete_cookie(key="access_token")
    return response


@router.get("/tracks", response_class=HTMLResponse)
@template("tracks.html")
async def tracks_page(
    request: Request,
    session: Session = Depends(db.get_session),
):
    tracks = await get_tracks(session)
    return {"tracks": tracks}


@router.get("/tracks/{track_id}", response_class=HTMLResponse)
@template("track.html")
async def track_page(
    track_id: int,
    request: Request,
    session: Session = Depends(db.get_session),
):
    track = await get_track(track_id, session)
    return {"track": track}


@router.get("/albums", response_class=HTMLResponse)
@template("albums.html")
async def albums_page(
    request: Request,
    session: Session = Depends(db.get_session),
):
    albums = await get_albums(session)
    return {"albums": albums}


@router.get("/albums/{album_id}", response_class=HTMLResponse)
@template("album.html")
async def album_page(
    album_id: int,
    request: Request,
    session: Session = Depends(db.get_session),
):
    album = get_single_or_404(session, Album, album_id)
    return {"album": album, "tracks": album.tracks}


@router.get("/explore", response_class=HTMLResponse)
@template("explore.html")
async def explore_page(
    request: Request,
    session: Session = Depends(db.get_session),
):
    albums = session.exec(select(Album)).all()
    tracks = session.exec(select(Track)).all()

    return {"tracks": tracks, "albums": albums}


@router.get("/upload", response_class=HTMLResponse)
@template("upload.html")
async def upload_page(
    request: Request,
    session: Session = Depends(db.get_session),
    user: UserRead = Depends(get_user_required),
):
    pass


@router.get("/users/{username}", response_class=HTMLResponse)
@template("user.html")
async def user_page(
    username: str,
    request: Request,
    session: Session = Depends(db.get_session),
):
    user_to_display = get_single_or_404(session, User, username)
    return {"user_to_display": user_to_display}
