"""Spotipy routes for user-authentication"""
from datetime import datetime, timedelta
from typing import Annotated

from fastapi import APIRouter, Depends, Form, HTTPException, status
from fastapi.responses import RedirectResponse
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import JWTError, jwt
from passlib.context import CryptContext
from sqlmodel import Session

from .. import db
from ..models import TokenData, User
from .core import ACCESS_TOKEN_EXPIRE_MINUTES, ALGORITHM, SECRET_KEY, user_exists

router = APIRouter()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/login")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_password_hash(password: str) -> str:
    return pwd_context.hash(password)


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(plain_password, hashed_password)


def create_access_token(data: dict, expires_delta: timedelta | None = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


def authenticate_user(session: Session, username: str, password: str):
    user = session.get(User, username)

    if not user:
        return False
    if not verify_password(password, user.hashed_password):
        return False
    return user


async def get_current_user(token: Annotated[str, Depends(oauth2_scheme)]):
    credentials_exception = HTTPException(
        status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        username = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = TokenData(username=username)
    except JWTError:
        raise credentials_exception
    with Session(db.engine) as session:
        user = session.get(User, token_data.username)
    if user is None:
        raise credentials_exception
    return user


def login_user(session: Session, username: str, password: str):
    """Do a user login"""
    user = authenticate_user(session, username, password)

    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    response = RedirectResponse(url="/", status_code=status.HTTP_302_FOUND)
    response.set_cookie(key="access_token", value=access_token)
    return response


@router.post("/api/register", response_class=RedirectResponse)
def register(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
    display_name: Annotated[str, Form()],
    session: Session = Depends(db.get_session),
):
    """User registration"""
    if user_exists(session, form_data.username):
        raise HTTPException(
            status_code=status.HTTP_409_CONFLICT, detail="Username already registered"
        )

    new_user = User(
        username=form_data.username,
        display_name=display_name,
        hashed_password=get_password_hash(form_data.password),
    )

    session.add(new_user)
    session.commit()

    return login_user(session, form_data.username, form_data.password)


@router.get("/api/register/valid")
def check_register(
    username: str,
    session: Session = Depends(db.get_session),
):
    if len(username) < 3:
        return False
    return not user_exists(session, username)


@router.post("/api/login", response_class=RedirectResponse)
async def login(
    form_data: Annotated[OAuth2PasswordRequestForm, Depends()],
    session: Session = Depends(db.get_session),
):
    return login_user(session, form_data.username, form_data.password)
