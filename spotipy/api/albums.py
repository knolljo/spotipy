"""Spotipy routes for albums"""
from typing import Annotated

from fastapi import APIRouter, Depends, File, Form, UploadFile, status, HTTPException
from fastapi.responses import RedirectResponse
from sqlmodel import Session

from .. import db
from ..models import Album, AlbumRead, UserRead, TrackCreate, Track
from .core import get_all_or_404, get_single_or_404, get_user_required, save_files

router = APIRouter()


@router.get("/api/albums", response_model=list[AlbumRead])
async def get_albums(session: Session = Depends(db.get_session)):
    """Get all albums"""
    return get_all_or_404(session, Album)


@router.get("/api/albums/{album_id}", response_model=AlbumRead)
async def get_album(album_id: int, session: Session = Depends(db.get_session)):
    """Get one album by id"""
    return get_single_or_404(session, Album, album_id)


@router.post("/api/albums", status_code=201)
async def create_album(
    title: Annotated[str, Form()],
    track_titles: list[str],
    files: list[UploadFile] = File(...),
    session: Session = Depends(db.get_session),
    user: UserRead = Depends(get_user_required),
):
    """Create album"""
    titles = track_titles[0].split(",")

    if len(titles) != len(files):
        raise HTTPException(status.HTTP_400_BAD_REQUEST, "Invalid album creation")

    paths = await save_files(files)

    new_album = Album(title=title, username=user.username)
    session.add(new_album)
    session.commit()

    tracks = [
        Track.from_orm(
            TrackCreate(
                title=title,
                album_id=new_album.id,
                audio_url=path.name,
                username=user.username,
            )
        )
        for title, path in zip(titles, paths)
    ]

    session.add_all(tracks)
    session.commit()

    return RedirectResponse(
        url=f"/albums/{new_album.id}", status_code=status.HTTP_302_FOUND
    )
