"""Core functions"""
import asyncio
import hashlib
from pathlib import Path
from typing import Optional

import aiofiles
from fastapi import HTTPException, Request, UploadFile, status
from jose import JWTError, jwt
from sqlmodel import Session, SQLModel, select

from .. import db
from ..models import User, UserRead

SECRET_KEY = "be0a987e90ee7a73bc13abb769c26168991a0ef2d40d049cd29546a5ab944cf5"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30


def get_user_optional(request: Request) -> Optional[UserRead]:
    access_token = request.cookies.get("access_token")
    if not access_token:
        return None
    try:
        payload = jwt.decode(access_token, SECRET_KEY, algorithms=[ALGORITHM])
        username = payload.get("sub")
        with Session(db.engine) as session:
            return UserRead.from_orm(session.get(User, username))
    except JWTError:
        return None


def get_user_required(request: Request) -> UserRead:
    if user := get_user_optional(request):
        return user
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )


def user_exists(session: Session, username: str) -> bool:
    if session.get(User, username):
        return True
    return False


def get_single_or_404(session: Session, model: type[SQLModel], entry_id: int | str):
    if entry := session.get(model, entry_id):
        return entry
    raise HTTPException(status.HTTP_404_NOT_FOUND, detail=f"{model.__name__} not found")


def get_all_or_404(session: Session, model: type[SQLModel]):
    if entries := session.exec(select(model)).all():
        return entries
    raise HTTPException(status.HTTP_404_NOT_FOUND, detail=f"No {model.__name__}s found")


UPLOAD_DIR = Path.cwd() / "audio"
UPLOAD_DIR.mkdir(exist_ok=True, parents=True)


def sha256sum(file_content: bytes):
    sha256 = hashlib.sha1()
    sha256.update(file_content)
    return sha256.hexdigest()


async def save_file(file: UploadFile) -> Path:
    if file.content_type != "audio/mpeg":
        raise HTTPException(
            status.HTTP_400_BAD_REQUEST, detail="Only MP3 files are allowed."
        )
    file_content = file.file.read()
    hash = sha256sum(file_content)
    upload_path = (UPLOAD_DIR / hash).with_suffix(".mp3")

    async with aiofiles.open(str(upload_path), "wb") as file:
        await file.write(file_content)
    return upload_path


async def save_files(files: list[UploadFile]) -> list[Path]:
    # async with asyncio.TaskGroup() as tg:
    #     for file in files:
    #         tg.create_task(save_file(file))

    results = await asyncio.gather(*(save_file(file) for file in files))
    return list(results)
