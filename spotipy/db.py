"""Database models and mappings of Spotipy"""
from pathlib import Path

from sqlalchemy.ext.asyncio import create_async_engine
from sqlalchemy.orm import sessionmaker
from sqlmodel import Session, SQLModel, create_engine
from sqlmodel.ext.asyncio.session import AsyncSession

sqlite_file = "spotipy.db"
sqlite_url = f"sqlite:///{sqlite_file}"
async_sqlite_url = f"sqlite+aiosqlite:///{sqlite_file}"
engine = create_engine(sqlite_url, echo=True, connect_args={"check_same_thread": False})


def get_session():
    with Session(engine) as session:
        yield session


def create_tables():
    if not Path(sqlite_file).exists():
        SQLModel.metadata.create_all(engine)


async_engine = create_async_engine(async_sqlite_url, echo=True, future=True)


async def get_async_session() -> AsyncSession:
    async_session = sessionmaker(
        bind=async_engine, class_=AsyncSession, expire_on_commit=False
    )
    async with async_session() as session:
        yield session
