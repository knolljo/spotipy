var searchQueryInput = document.getElementById('search');
var searchResultsDiv = document.getElementById('search-results');
var selectedResult = null;

searchQueryInput.addEventListener('keyup', handleSearchQueryChange);
searchQueryInput.addEventListener('change', handleSearchQueryChange);
searchQueryInput.addEventListener('submit', handleSearchSubmit);

function handleSearchQueryChange() {
  var searchQuery = searchQueryInput.value;
  var url = '/api/search?title=' + encodeURIComponent(searchQuery);

  fetch(url)
    .then(function(response) {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error('Error: ' + response.status);
      }
    })
    .then(function(results) {
      displayResults(results);
    })
    .catch(function(error) {
      console.error(error);
    });
}

function displayResults(results) {
  searchResultsDiv.innerHTML = ''; // Clear previous results

  if (results.length > 0) {
    searchResultsDiv.style.display = 'block';

    for (var i = 0; i < results.length; i++) {
      var result = results[i];
      var resultDiv = document.createElement('div');
      resultDiv.textContent = result.title;
      resultDiv.addEventListener('click', selectResult);
      searchResultsDiv.appendChild(resultDiv);
    }
  } else {
    searchResultsDiv.style.display = 'none';
  }
}

function selectResult(event) {
  selectedResult = event.target.textContent;
  searchQueryInput.value = selectedResult;
  searchResultsDiv.style.display = 'none';
}

function handleSearchSubmit(event) {
  event.preventDefault();

  if (selectedResult !== null) {
    window.location.href = "/tracks/" + encodeURIComponent(selectedResult);
  }
}
